package schwarz.similarities;

import schwarz.datamodel.Index;

public class TfIdf extends Similarity {

	public TfIdf(Index index) {
		super(index);
	}

	@Override
	double getScore(String term, int docId, int termFreqQ) {
		return index.getNormalizedTf(term, docId) * index.getNormalizedIdf(term);
	}

}
