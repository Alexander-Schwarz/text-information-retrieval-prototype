package schwarz.similarities;

import schwarz.datamodel.Index;
import schwarz.datamodel.PostingListEntry;
import schwarz.datamodel.Scoring;
import schwarz.datamodel.Topic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Similarity {

	Index index;

	public Similarity(Index index) {
		this.index = index;
	}

	abstract double getScore(String term, int docId, int termFreqQ);

	public List<Scoring> getSortedScoringListForTopic(Topic topic) {
		List<Scoring> scoringList = new ArrayList<>();

		for (String term : topic.getTerms()) {
			combineScoringList(scoringList, getScoringListForTerm(term, topic.getTermFreqQ(term)));
		}

		// sort final list by score and not by docId
		scoringList.sort(Scoring::compareScore);
		Collections.reverse(scoringList);

		if (scoringList.size() > 1000) {
			return scoringList.subList(0, 1000);
		} else {
			return scoringList;
		}
	}

	private List<Scoring> getScoringListForTerm(String term, int termFreqQ) {
		List<Scoring> scoringList = new ArrayList<>();

		if (index.getPostingList(term) == null) {
			return scoringList;
		}

		for (PostingListEntry e : index.getPostingList(term).getList()) {
			scoringList.add(new Scoring(e.getDocId(), getScore(term, e.getDocId(), termFreqQ)));
		}

		// scoring list has the same docId order as posting list
		return scoringList;
	}

	private void combineScoringList(List<Scoring> main, List<Scoring> additional) {

		// iterate over each element of the additional scoring list and merge it with the main list
		for (Scoring score : additional) {
			addScoreToScoringList(main, score);
		}

	}

	private void addScoreToScoringList(List<Scoring> list, Scoring score) {
		if (list.isEmpty()) {
			list.add(score);
			return;
		}

		for (int i = list.size() - 1; i >= 0; i--) {
			Scoring cur = list.get(i);

			int comp = cur.compareToDocId(score.getDocId());

			if (comp > 0) {
				continue;
			}

			if (comp == 0) {
				cur.addScore(score.getScore());
				return;
			}

			if (i == list.size() - 1) {
				list.add(score);
				return;
			} else {
				list.add(i + 1, score);
				return;
			}
		}

		list.add(0, score);
	}
}
