package schwarz.similarities;

import schwarz.datamodel.Index;

public class BM25VA extends Similarity {

	private double k1;
	private double k3;

	public BM25VA(Index index, double k1, double k3) {
		super(index);
		this.k1 = k1;
		this.k3 = k3;
	}

	@Override
	double getScore(String term, int docId, int termFreqQ) {
		// formula from: Verboseness Fission for BM25 Document Length Normalization (10.1145/2808194.2809486)

		double bva = Math.pow(index.getMavgtf(), -2) * (index.getDocLength(docId) / index.getDocTermCount(docId));
		bva += (1 - Math.pow(index.getMavgtf(), -1)) * (index.getDocLength(docId) / index.getAverageDocLength());

		double tfd = index.getTf(term, docId) / bva;

		double bm25_value = (k3 + 1) * termFreqQ / (k3 + termFreqQ);
		bm25_value *= ((k1 + 1) * tfd) / (k1 + tfd);
		bm25_value *= Math.log10((index.getDocCount() + 0.5) / (index.getDf(term) + 0.5));
		return bm25_value;
	}
}
