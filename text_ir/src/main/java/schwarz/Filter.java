package schwarz;

import opennlp.tools.lemmatizer.DictionaryLemmatizer;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.stemmer.PorterStemmer;
import opennlp.tools.stemmer.Stemmer;
import org.apache.commons.configuration2.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Filter {

	// based off of https://github.com/igorbrigadir/stopwords/blob/master/en/postgresql.txt
	private final List<String> stopWordsList = Arrays.asList(
			"i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself", "yourselves",
			"he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself", "they", "them", "their",
			"theirs", "themselves", "what", "which", "who", "whom", "this", "that", "these", "those", "am", "is", "are", "was",
			"were", "be", "been", "being", "have", "has", "had", "having", "do", "does", "did", "doing", "a", "an", "the",
			"and", "but", "if", "or", "because", "as", "until", "while", "of", "at", "by", "for", "with", "about", "against",
			"between", "into", "through", "during", "before", "after", "above", "below", "to", "from", "up", "down", "in",
			"out", "on", "off", "over", "under", "again", "further", "then", "once", "here", "there", "when", "where", "why",
			"how", "all", "any", "both", "each", "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only",
			"own", "same", "so", "than", "too", "very", "s", "t", "can", "will", "just", "don", "should", "now"
	);

	private boolean extractSpecialRegexStrings = true;
	private final String emailRegex = "[a-z0-9_.+-]+@[a-z0-9-]+\\.[a-z0-9-.]+";
	private final Pattern specialStringRegex = Pattern.compile(emailRegex);

	private Stemmer stemmer;
	private DictionaryLemmatizer lemmatizer;
	private POSTaggerME tagger;

	public Filter(Configuration config) {
		extractSpecialRegexStrings = config.getBoolean("extractSpecialRegexStrings");

		stemmer = new PorterStemmer();

		// dictionary from: https://raw.githubusercontent.com/richardwilly98/elasticsearch-opennlp-auto-tagging/master/src/main/resources/models/en-lemmatizer.dict
		InputStream is = getClass().getResourceAsStream("/en-lemmatizer.dict");
		// POS model from: http://opennlp.sourceforge.net/models-1.5/en-pos-maxent.bin
		InputStream posModelStream = getClass().getResourceAsStream("/en-pos-maxent.bin");

		try {
			lemmatizer = new DictionaryLemmatizer(is);
			tagger = new POSTaggerME(new POSModel(posModelStream));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public List<String> getTerms(String text, boolean caseFold, boolean stopWord, boolean stemWord, boolean lemWord) {

		List<String> terms = new LinkedList<>();

		text = text.trim();

		if (caseFold) {
			text = text.toLowerCase();
		}

		if (extractSpecialRegexStrings) {
			Matcher m = specialStringRegex.matcher(text);
			while (m.find()) {
				System.out.println(m.group());
				terms.add(m.group());
			}
		}

		// split on all non word characters except hyphen
		ListIterator<String> splitIterator = new ArrayList<String>(Arrays.asList(text.split("[^a-zA-Z0-9-]+"))).listIterator();

		while (splitIterator.hasNext()) {
			String s = splitIterator.next();

			if (s.length() == 0) {
				continue;
			}

			// if s is a hyphenated word append the split and joined words
			if (s.contains("-")) {

				// check that word does not only consist of hyphens
				boolean containsOtherChar = false;
				for (char c : s.toCharArray()) {
					if (c != '-') {
						containsOtherChar = true;
						break;
					}
				}

				if (containsOtherChar) {
					for (String hyphensplit : Arrays.asList(s.split("-"))) {
						splitIterator.add(hyphensplit);
					}
					splitIterator.add(s.replace("-", ""));
				} else {
					continue;
				}
			}

			if (stopWord) {
				s = removeStopWord(s);
				if (s == null) {
					continue;
				}
			}

			if (stemWord) {
				s = stemWord(s);
			}

			if (lemWord) {
				s = lemWord(s);
			}

			terms.add(s);
		}

		return terms;
	}

	private String removeStopWord(String word) {
		if (stopWordsList.contains(word)) {
			return null;
		} else {
			return word;
		}
	}

	private String stemWord(String word) {
		return stemmer.stem(word).toString();
	}

	private String lemWord(String word) {
		String lemmatized = lemmatizer.lemmatize(new String[]{word}, tagger.tag(new String[]{word}))[0];

		// lematizer returns "O" if no matching word/POS-tag was found
		if (!lemmatized.equals("O")) {
			return lemmatized;
		} else {
			return word;
		}
	}


}
