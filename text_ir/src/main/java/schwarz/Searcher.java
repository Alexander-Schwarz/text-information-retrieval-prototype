package schwarz;

import org.apache.commons.cli.*;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.nustaq.serialization.FSTConfiguration;
import org.nustaq.serialization.FSTObjectInput;
import schwarz.datamodel.*;
import schwarz.similarities.BM25;
import schwarz.similarities.BM25VA;
import schwarz.similarities.Similarity;
import schwarz.similarities.TfIdf;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class Searcher {

	private static final String SIMILARITY = "similarity";
	private static final String INDEX_PATH = "index";
	private static final String TOPIC_PATH = "topic";
	private static final String OUTPUT = "output";
	private static final String K1 = "k1";
	private static final String K3 = "k3";
	private static final String B = "b";

	private static final String TFIDF = "tfidf";
	private static final String BM25 = "bm25";
	private static final String BM25VA = "bm25va";

	private static CommandLine cmd;

	private static Index index;
	private static Similarity similarity;
	private static Filter filter;

	private static FSTConfiguration fstConfiguration;
	private static Configuration config;

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);

		parseArguments(args);

		Configurations configs = new Configurations();
		try {
			config = configs.properties(new File("config.properties"));
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}

		fstConfiguration = FSTConfiguration.createDefaultConfiguration();
		fstConfiguration.registerClass(Index.class, PostingList.class, PostingListEntry.class);

		filter = new Filter(config);
		List<Topic> topicList;

		long startTime = System.nanoTime();

		try {
			loadIndex(cmd.getOptionValue(INDEX_PATH));

			similarity = getSimilarity();
			topicList = parseTopics();

			// remove previous result file if it exists
			if (cmd.hasOption(OUTPUT)) {
				File previousResultFile = new File(cmd.getOptionValue(OUTPUT));
				if (previousResultFile.exists()) {
					previousResultFile.delete();
				}
			}

			searchTopics(topicList);
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(String.format("Overall searching process finished after %d seconds.", (System.nanoTime() - startTime) / 1_000_000_000L));
	}

	private static void parseArguments(String[] args) {
		Options options = new Options();
		Option similarityOption = new Option("s", SIMILARITY, true, "similarity functions: tfidf, bm25, bm25va");
		similarityOption.setRequired(true);
		options.addOption(similarityOption);

		Option indexPath = new Option("i", INDEX_PATH, true, "path to index file");
		indexPath.setRequired(true);
		options.addOption(indexPath);

		Option topicPath = new Option("t", TOPIC_PATH, true, "path to topic file");
		topicPath.setRequired(true);
		options.addOption(topicPath);

		Option outputFolder = new Option("o", OUTPUT, true, "path for result file");
		options.addOption(outputFolder);

		options.addOption(new Option("k1", K1, true, "k1 parameter for BM25/VA"));
		options.addOption(new Option("k3", K3, true, "k3 parameter for BM25/VA"));
		options.addOption(new Option("b", B, true, "b parameter for BM25"));

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();

		try {
			cmd = parser.parse(options, args);

			switch (cmd.getOptionValue(SIMILARITY)) {

				case TFIDF:
					if (cmd.hasOption(K1) || cmd.hasOption(K3) || cmd.hasOption(B)) {
						throw new ParseException("TF-IDF does not have any parameters.");
					}
					break;

				case BM25:
					if (!cmd.hasOption(K1) || !cmd.hasOption(K3) || !cmd.hasOption(B)) {
						throw new ParseException("BM25 requires parameters: k1, k3 and b");
					}
					break;

				case BM25VA:
					if (!cmd.hasOption(K1) || !cmd.hasOption(K3)) {
						throw new ParseException("BM25 requires parameters: k1, k3");
					}
					break;

				default:
					throw new ParseException("Similarity has to be: tfidf, bm25 or bm25v");
			}

		} catch (ParseException e) {
			System.err.println(e.getMessage());
			formatter.printHelp("searcher", options);

			System.exit(1);
		}

		System.out.println("Arguments:");
		for (Option o : cmd.getOptions()) {
			System.out.println((o.getLongOpt() == null ? o.getOpt() : o.getLongOpt()) + ": " + (o.getValue() == null ? "true" : o.getValue()));
		}
	}

	private static List<Topic> parseTopics() throws IOException {
		List<Topic> topicList = new ArrayList<>();

		File topicFile = new File(cmd.getOptionValue(TOPIC_PATH));
		Document topicDocument = Jsoup.parse(topicFile, "ISO-8859-1");
		Elements topics = topicDocument.select("top");

		for (Element top : topics) {
			// parsing of elements not working entirely automatic due to wrong placement of closing tags
			String num = top.selectFirst("num").toString().replace("<num>", "").replace("Number:", "");
			int topicId = Integer.parseInt(num.substring(0, num.indexOf("<")).trim());

			String title = top.selectFirst("title").toString().replace("<title>", "");
			title = title.substring(0, title.indexOf("<"));

			// filter setting has to match initial index creation for best results
			List<String> terms = filter.getTerms(title, index.isCaseFold(), index.isStopWord(), index.isStemWord(), index.isLemWord());

			topicList.add(new Topic(topicId, terms));
		}

		return topicList;
	}

	private static void searchTopics(List<Topic> topicList) throws IOException {
		for (Topic t : topicList) {
			List<Scoring> scoringList = similarity.getSortedScoringListForTopic(t);
			formatScoringList(t, scoringList);
		}
	}

	private static void formatScoringList(Topic topic, List<Scoring> scoringList) throws IOException {
		PrintWriter pw = null;

		if (cmd.hasOption(OUTPUT)) {
			FileWriter fw = new FileWriter(cmd.getOptionValue(OUTPUT), true);
			BufferedWriter bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);
		}

		int ranking = 1;
		for (Scoring s : scoringList) {
			String line = String.format("%d Q0 %s %d %f assign1_%s", topic.getTopicId(), index.getDocNoForDocId(s.getDocId()), ranking, s.getScore(), cmd.getOptionValue(SIMILARITY));
			ranking++;

			if (pw != null) {
				pw.println(line);
			} else {
				System.out.println(line);
			}
		}

		if (pw != null) {
			pw.flush();
			pw.close();
		}
	}

	private static void loadIndex(String indexPath) throws IOException {
		System.out.println("Loading index ...");
		long startTime = System.nanoTime();

		FileInputStream fis = new FileInputStream(indexPath);
		FSTObjectInput fstIn = fstConfiguration.getObjectInput(fis);

		try {
			index = (Index) fstIn.readObject(Index.class);
			fstIn.close();
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println(String.format("Index loading finished after %d seconds.", (System.nanoTime() - startTime) / 1_000_000_000L));
	}

	private static Similarity getSimilarity() {
		switch (cmd.getOptionValue(SIMILARITY)) {
			case TFIDF:
				return new TfIdf(index);
			case BM25:
				return new BM25(
						index,
						Double.parseDouble(cmd.getOptionValue(K1)),
						Double.parseDouble(cmd.getOptionValue(K3)),
						Double.parseDouble(cmd.getOptionValue(B))
				);
			default:
				return new BM25VA(
						index,
						Double.parseDouble(cmd.getOptionValue(K1)),
						Double.parseDouble(cmd.getOptionValue(K3))
				);
		}
	}


}
