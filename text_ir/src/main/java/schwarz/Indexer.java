package schwarz;

import org.apache.commons.cli.*;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.nustaq.serialization.FSTConfiguration;
import org.nustaq.serialization.FSTObjectInput;
import org.nustaq.serialization.FSTObjectOutput;
import schwarz.datamodel.Index;
import schwarz.datamodel.IndexBlock;
import schwarz.datamodel.PostingList;
import schwarz.datamodel.PostingListEntry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;


public class Indexer {

	private static final String CASE_FOLDING = "fold";
	private static final String STOP_WORDS = "stop";
	private static final String STEMMING = "stem";
	private static final String LEMMATIZATION = "lem";
	private static final String INPUT = "input";
	private static final String OUTPUT = "output";

	private static CommandLine cmd;
	private static Filter filter;

	private static Index index;
	private static Map<Integer, String> docIdMap;
	private static IndexBlock indexBlock;
	private static List<File> blocksOnDisk;

	private static int FILES_PER_BLOCK;

	private static FSTConfiguration fstConfiguration;
	private static Configuration config;

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);

		parseArguments(args);

		Configurations configs = new Configurations();
		try {
			config = configs.properties(new File("config.properties"));
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}

		FILES_PER_BLOCK = config.getInt("filesPerBlock");

		fstConfiguration = FSTConfiguration.createDefaultConfiguration();
		fstConfiguration.registerClass(Index.class, PostingList.class, PostingListEntry.class, IndexBlock.class);

		filter = new Filter(config);
		indexBlock = new IndexBlock();
		blocksOnDisk = new LinkedList<>();
		docIdMap = new HashMap<>();
		index = new Index();

		long startTime = System.nanoTime();
		try {
			processFiles();
			createFinalIndex();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(String.format("Indexing process finished after %d seconds.", (System.nanoTime() - startTime) / 1_000_000_000L));
	}

	private static void parseArguments(String[] args) {
		Options options = new Options();
		options.addOption(new Option(CASE_FOLDING, "case folding"));
		options.addOption(new Option(STOP_WORDS, "stop word removal"));
		options.addOption(new Option(STEMMING, "stemming"));
		options.addOption(new Option(LEMMATIZATION, "lemmatization"));

		Option inputFolder = new Option("i", INPUT, true, "directory with input files");
		inputFolder.setRequired(true);
		options.addOption(inputFolder);
		Option outputFolder = new Option("o", OUTPUT, true, "directory for index");
		outputFolder.setRequired(true);
		options.addOption(outputFolder);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			formatter.printHelp("indexer", options);

			System.exit(1);
		}

		System.out.println("Arguments:");
		for (Option o : cmd.getOptions()) {
			System.out.println((o.getLongOpt() == null ? o.getOpt() : o.getLongOpt()) + ": " + (o.getValue() == null ? "true" : o.getValue()));
		}
	}

	private static void processFiles() throws IOException {
		Collection<File> fileList = FileUtils.listFiles(new File(cmd.getOptionValue(INPUT)), null, true);

		System.out.println(String.format("Found %d files in the input directory.", fileList.size()));
		int fileCount = 0;
		int filesInMem = 0;
		int docCount = 0;

		for (File f : fileList) {
			fileCount++;
			filesInMem++;

			if (fileCount % 10 == 0) {
				System.out.println(String.format("Indexed files: %d/%d | indexed documents: %d", fileCount, fileList.size(), docCount));
			}

			if (filesInMem >= FILES_PER_BLOCK) {
				saveBlockToDisk();
				filesInMem = 0;
			}

			Document documentFile = Jsoup.parse(f, "ISO-8859-1");
			Elements docs = documentFile.select("doc");

			for (Element doc : docs) {
				docCount++;

				if (doc.selectFirst("text") == null) {
					continue;
				}

				// use current docCount as new docId und save string representation in map for lookup
				docIdMap.put(docCount, doc.selectFirst("docno").text());

				// combines and normalizes text from itself and children
				String text = doc.selectFirst("text").text();

				processDocument(docCount, text);
			}
		}

		if (fileCount % 10 != 0) {
			System.out.println(String.format("Indexed files: %d/%d | indexed documents: %d", fileCount, fileList.size(), docCount));
		}

		// save remaining memory block to disk
		saveBlockToDisk();

		index.setDocCount(docCount);
	}

	private static void processDocument(Integer docId, String text) {

		List<String> terms = filter.getTerms(
				text,
				cmd.hasOption(CASE_FOLDING),
				cmd.hasOption(STOP_WORDS),
				cmd.hasOption(STEMMING),
				cmd.hasOption(LEMMATIZATION)
		);

		for (String term : terms) {
			indexBlock.addToDictionary(docId, term);
		}

		if (terms.size() > 0) {
			index.addDocLength(docId, terms.size());
			index.calculateAndAddDocTermCount(docId, terms);
		}
	}

	private static void saveBlockToDisk() {
		File block = new File(cmd.getOptionValue(OUTPUT), "block_" + blocksOnDisk.size());

		try {
			FileOutputStream fos = new FileOutputStream(block);
			FSTObjectOutput fstOut = fstConfiguration.getObjectOutput(fos);
			fstOut.writeObject(indexBlock, IndexBlock.class);
			fstOut.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		indexBlock = new IndexBlock();
		blocksOnDisk.add(block);
	}

	private static void createFinalIndex() throws IOException {
		System.out.println("Combining blocks to final index ...");

		for (File block : blocksOnDisk) {
			FileInputStream fis = new FileInputStream(block);
			FSTObjectInput fstIn = fstConfiguration.getObjectInput(fis);

			IndexBlock other = null;
			try {
				other = (IndexBlock) fstIn.readObject(IndexBlock.class);
				fstIn.close();
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			index.mergeIndexBlock(other);
			Files.delete(block.toPath());
		}
		blocksOnDisk.clear();

		index.setDocIdMap(docIdMap);

		index.setCaseFold(cmd.hasOption(CASE_FOLDING));
		index.setStopWord(cmd.hasOption(STOP_WORDS));
		index.setStemWord(cmd.hasOption(STEMMING));
		index.setLemWord(cmd.hasOption(LEMMATIZATION));

		System.out.println("Calculating statistics for index ...");
		index.calculateStatistics();

		System.out.println("Writing index to disk ...");
		File indexFile = new File(cmd.getOptionValue(OUTPUT), "index.ser");

		FileOutputStream fos = new FileOutputStream(indexFile);
		FSTObjectOutput fstOut = fstConfiguration.getObjectOutput(fos);
		fstOut.writeObject(index, Index.class);
		fstOut.close();
		fos.close();
	}


}
