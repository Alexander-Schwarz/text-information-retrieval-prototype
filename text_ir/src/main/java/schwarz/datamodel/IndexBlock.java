package schwarz.datamodel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class IndexBlock implements Serializable {

	private Map<String, PostingList> dictionary;

	public IndexBlock() {
		this.dictionary = new HashMap<>();
	}

	public Map<String, PostingList> getDictionary() {
		return dictionary;
	}

	public void addToDictionary(int docId, String term) {
		if (dictionary.containsKey(term)) {
			dictionary.get(term).addOccurrences(docId);
		} else {
			dictionary.put(term, new PostingList(docId));
		}
	}

	@Override
	public String toString() {
		return "IndexBlock{" +
				"dictionary=" + dictionary +
				'}';
	}
}
