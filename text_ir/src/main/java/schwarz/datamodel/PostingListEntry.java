package schwarz.datamodel;

import java.io.Serializable;
import java.util.Objects;

public class PostingListEntry implements Comparable, Serializable {

	private int docId;
	private int termFreq;

	public PostingListEntry(int docId) {
		this.docId = docId;
	}

	public PostingListEntry(int docId, int termFreq) {
		this.docId = docId;
		this.termFreq = termFreq;
	}

	public int getDocId() {
		return docId;
	}

	public void setDocId(int docId) {
		this.docId = docId;
	}

	public int getTermFreq() {
		return termFreq;
	}

	public void setTermFreq(int termFreq) {
		this.termFreq = termFreq;
	}

	public void addTermFreq(int termFreq) {
		this.termFreq += termFreq;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PostingListEntry that = (PostingListEntry) o;
		return docId == that.docId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(docId);
	}

	@Override
	public int compareTo(Object o) {
		int oId = ((PostingListEntry) o).getDocId();
		return (this.docId > oId) ? 1 : (this.docId < oId) ? -1 : 0;
	}

	public int compareToDocId(int otherDocId) {
		return (this.docId > otherDocId) ? 1 : (this.docId < otherDocId) ? -1 : 0;
	}

	@Override
	public String toString() {
		return "PostingListEntry{" +
				"docId='" + docId + '\'' +
				", termFreq=" + termFreq +
				'}';
	}
}
