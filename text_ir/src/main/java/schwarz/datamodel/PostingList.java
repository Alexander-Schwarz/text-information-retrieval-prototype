package schwarz.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PostingList implements Serializable {

	// posting list stays always sorted
	private List<PostingListEntry> list;

	public PostingList() {
		this.list = new ArrayList<>();
	}

	public PostingList(int docId) {
		this.list = new ArrayList<>();
		this.addOccurrences(docId);
	}

	public PostingList(List<PostingListEntry> list) {
		this.list = list;
	}

	public List<PostingListEntry> getList() {
		return list;
	}

	public int getTfForDocId(int docId) {
		for (PostingListEntry entry : list) {
			if (entry.getDocId() == docId) {
				return entry.getTermFreq();
			}
		}
		return 0;
	}

	public int size() {
		return list.size();
	}

	public void addOccurrences(int docId, int newFreq) {

		if (list.isEmpty()) {
			list.add(new PostingListEntry(docId, newFreq));
			return;
		}

		// iterate over list from back to front since docIds are assigned in ascending order and posting lists are sorted in ascending order
		for (int i = list.size() - 1; i >= 0; i--) {
			PostingListEntry cur = list.get(i);

			int comp = cur.compareToDocId(docId);

			if (comp > 0) {
				continue;
			}

			if (comp == 0) {
				cur.addTermFreq(newFreq);
				return;
			}

			// prevent IndexOutOfBoundsException if docId is greater than every id in the list
			if (i == list.size() - 1) {
				list.add(new PostingListEntry(docId, newFreq));
				return;
			} else {
				list.add(i + 1, new PostingListEntry(docId, newFreq));
				return;
			}
		}

		// new docId is smaller than every id in the list
		list.add(0, new PostingListEntry(docId, newFreq));
	}

	public void addOccurrences(int docId) {
		this.addOccurrences(docId, 1);
	}

	public PostingList combine(PostingList other) {
		PostingList combinedPostingList = new PostingList(new ArrayList<>(list));

		for (PostingListEntry entry : other.getList()) {
			combinedPostingList.addOccurrences(entry.getDocId(), entry.getTermFreq());
		}
		return combinedPostingList;
	}

	@Override
	public String toString() {
		return "PostingList{" +
				"list=" + list +
				'}';
	}
}
