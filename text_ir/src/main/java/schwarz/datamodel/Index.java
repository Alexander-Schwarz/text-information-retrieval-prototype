package schwarz.datamodel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Index implements Serializable {

	private Map<String, PostingList> invertedIndex;
	private Map<Integer, String> docIdMap;
	private Map<Integer, Integer> docLengthMap;
	private Map<Integer, Integer> docTermCount;

	// save options during indexing so search process can reuse them
	private boolean caseFold = false;
	private boolean stopWord = false;
	private boolean stemWord = false;
	private boolean lemWord = false;

	private int docCount;
	private double averageDocLength = 0d;
	private double mavgtf = 0d;

	public Index() {
		this.invertedIndex = new HashMap<>();
		this.docLengthMap = new HashMap<>();
		this.docTermCount = new HashMap<>();
	}

	public Map<String, PostingList> getInvertedIndex() {
		return invertedIndex;
	}

	public void setInvertedIndex(Map<String, PostingList> invertedIndex) {
		this.invertedIndex = invertedIndex;
	}

	public Map<Integer, String> getDocIdMap() {
		return docIdMap;
	}

	public void setDocIdMap(Map<Integer, String> docIdMap) {
		this.docIdMap = docIdMap;
	}

	public int getDocCount() {
		return docCount;
	}

	public void setDocCount(int docCount) {
		this.docCount = docCount;
	}

	public int getDocLength(int docId) {
		return docLengthMap.get(docId);
	}

	public int getDocTermCount(int docId) {
		return docTermCount.get(docId);
	}

	public void addDocLength(int docId, int docLength) {
		docLengthMap.put(docId, docLength);
	}

	public double getMavgtf() {
		return mavgtf;
	}

	public double getAverageDocLength() {
		return averageDocLength;
	}

	public boolean isCaseFold() {
		return caseFold;
	}

	public void setCaseFold(boolean caseFold) {
		this.caseFold = caseFold;
	}

	public boolean isStopWord() {
		return stopWord;
	}

	public void setStopWord(boolean stopWord) {
		this.stopWord = stopWord;
	}

	public boolean isStemWord() {
		return stemWord;
	}

	public void setStemWord(boolean stemWord) {
		this.stemWord = stemWord;
	}

	public boolean isLemWord() {
		return lemWord;
	}

	public void setLemWord(boolean lemWord) {
		this.lemWord = lemWord;
	}

	public void mergeIndexBlock(IndexBlock block) {
		for (Entry<String, PostingList> entry : block.getDictionary().entrySet()) {
			if (!invertedIndex.containsKey(entry.getKey())) {
				invertedIndex.put(entry.getKey(), entry.getValue());
			} else {
				invertedIndex.put(entry.getKey(), invertedIndex.get(entry.getKey()).combine(entry.getValue()));
			}
		}
	}

	public void calculateStatistics() {

		// calculate average document length
		for (Entry<Integer, Integer> entry : docLengthMap.entrySet()) {
			averageDocLength += (double) entry.getValue();
		}
		averageDocLength /= (double) docCount;

		// calculate mean average term frequency
		for (Entry<Integer, Integer> entry : docTermCount.entrySet()) {
			mavgtf += docLengthMap.get(entry.getKey()) / (double) entry.getValue();
		}
		mavgtf /= (double) docCount;

	}

	public void calculateAndAddDocTermCount(int docId, List<String> terms) {
		docTermCount.put(docId, (new HashSet<String>(terms)).size());
	}

	public PostingList getPostingList(String term) {
		return invertedIndex.get(term);
	}

	public double getDf(String term) {
		return invertedIndex.get(term).size();
	}

	public double getIdf(String term) {
		return docCount / (double) invertedIndex.get(term).size();
	}

	public double getNormalizedIdf(String term) {
		return Math.log10(getIdf(term));
	}

	public double getTf(String term, int docId) {
		return invertedIndex.get(term).getTfForDocId(docId);
	}

	public double getNormalizedTf(String term, int docId) {
		return Math.log10(1 + getTf(term, docId));
	}

	public String getDocNoForDocId(int docId) {
		return docIdMap.get(docId);
	}

	@Override
	public String toString() {
		return "Index{" +
				"invertedIndex=" + invertedIndex +
				'}';
	}
}
