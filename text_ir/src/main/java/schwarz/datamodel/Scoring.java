package schwarz.datamodel;

import java.util.Objects;

public class Scoring implements Comparable {

	private int docId;
	private double score;

	public Scoring(int docId, double score) {
		this.docId = docId;
		this.score = score;
	}

	public int getDocId() {
		return docId;
	}

	public void setDocId(int docId) {
		this.docId = docId;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public void addScore(double additionalScore) {
		this.score += additionalScore;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Scoring scoring = (Scoring) o;
		return docId == scoring.docId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(docId);
	}

	@Override
	public int compareTo(Object o) {
		int oId = ((Scoring) o).getDocId();
		return (this.docId > oId) ? 1 : (this.docId < oId) ? -1 : 0;
	}

	public int compareToDocId(int otherDocId) {
		return (this.docId > otherDocId) ? 1 : (this.docId < otherDocId) ? -1 : 0;
	}

	public int compareScore(Scoring o) {
		return Double.compare(this.score, o.score);
	}

	@Override
	public String toString() {
		return "Scoring{" +
				"docId=" + docId +
				", score=" + score +
				'}';
	}
}
