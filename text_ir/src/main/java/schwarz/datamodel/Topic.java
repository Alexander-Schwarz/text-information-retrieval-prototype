package schwarz.datamodel;

import java.util.List;

public class Topic {

	private int topicId;
	private List<String> terms;

	public Topic(int topicId, List<String> terms) {
		this.topicId = topicId;
		this.terms = terms;
	}

	public int getTopicId() {
		return topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public List<String> getTerms() {
		return terms;
	}

	public void setTerms(List<String> terms) {
		this.terms = terms;
	}

	public int getTermFreqQ(String term) {
		int count = 0;
		for (String s : terms) {
			if (term.equals(s)) {
				count++;
			}
		}
		return count;
	}
}
